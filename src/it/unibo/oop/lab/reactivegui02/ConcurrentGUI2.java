package it.unibo.oop.lab.reactivegui02;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import it.unibo.oop.lab.reactivegui01.ConcurrentGUI;




public class ConcurrentGUI2 extends JFrame{
    private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel display = new JLabel();
    private final JButton stop = new JButton("stop");
    private final JButton up = new JButton("up");
    private final JButton down = new JButton ("down");
    
    public ConcurrentGUI2() {
        super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel();
        panel.add(display);
        panel.add(stop);
        panel.add(up);
        panel.add(down);
        this.getContentPane().add(panel);
        this.setVisible(true);   
        final Agent agent = new Agent();
        new Thread(agent).start();
        stop.addActionListener(new ActionListener() {       
            public void actionPerformed(final ActionEvent e) {
                // Agent should be final
                agent.stopCounting();
                stop.setEnabled(false);
                up.setEnabled(false);
                down.setEnabled(false); 
            }
        });
        
        up.addActionListener(new ActionListener() {       
            public void actionPerformed(final ActionEvent e) {
                // Agent should be final
                agent.upCounting();
            }
        });
        
        down.addActionListener(new ActionListener() {       
            public void actionPerformed(final ActionEvent e) {
                // Agent should be final
                agent.downCounting();
            }
        });
        
    }
    
    
    private class Agent implements Runnable {

        private volatile boolean stop;
        private volatile boolean up;
        private volatile boolean down;
        private  int counter;
		@Override
		public void run() {
            while (!this.stop) {
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {
                        @Override
                        public void run() {
                            ConcurrentGUI2.this.display.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                    if(this.up) {
                    this.counter++;
                    Thread.sleep(100);
                    }
                    else if (this.down) {
                	 this.counter--;
                     Thread.sleep(100);	
                    }
                } catch (InvocationTargetException | InterruptedException ex) {

                    ex.printStackTrace();
                }
            }	
    }
        public void stopCounting() {
            this.stop = true;
        }
        public void upCounting() {
            this.up = true;
            this.down = false;
        }
        public void downCounting() {
            this.down = true;
            this.up = false;
        }
    }
}